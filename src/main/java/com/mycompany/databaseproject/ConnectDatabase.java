/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author thisisfocus_k
 */
public class ConnectDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:Dcoffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (conn != null){
                try {
                    conn.close();
                }catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                } 
            }
        }
    
    }
}
